package firstgame.ranking;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;

//@Entity
//@Table(name = "scores")
public class RankingEntity {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "id")
//    private int id;
//
//    @Column(name = "player_nick")
//    private String playerNick;
//
//    @Column(name = "points")
//    private  int points;
//
//    @Column(name = "difficulty_level")
//    private String difficultyLevel;
//
//    public RankingEntity() {}
//
//    public RankingEntity(String playerNick, int points, String difficultyLevel) {
//        this.playerNick = playerNick;
//        this.points = points;
//        this.difficultyLevel = difficultyLevel;
//    }
//
//    public int getPlayerid() {
//        return id;
//    }
//
//    public String getPlayerNick() {
//        return playerNick;
//    }
//
//    public int getPoints() {
//        return points;
//    }
//
//    public String getDifficultyLevel() {
//        return difficultyLevel;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public void setPlayerNick(String playerNick) {
//        this.playerNick = playerNick;
//    }
//
//    public void setPoints(int points) {
//        this.points = points;
//    }
//
//    public void setDifficultyLevel(String difficultyLevel) {
//        this.difficultyLevel = difficultyLevel;
//    }

        //FIELDS
    private SimpleStringProperty nick;
    private SimpleDoubleProperty points;
    private SimpleStringProperty difficultyLvl;

    public RankingEntity(String nick, double points, String difficultyLvl) {
        this.nick = new SimpleStringProperty(nick);
        this.points = new SimpleDoubleProperty(points);
        this.difficultyLvl = new SimpleStringProperty(difficultyLvl);
    }

    public SimpleStringProperty getNick() {
        return nick;
    }

    public void setUsername(SimpleStringProperty nick) {
        this.nick = nick;
    }

    public SimpleDoubleProperty getPoints() {
        return points;
    }

    public void setPoints(SimpleDoubleProperty points) {
        this.points = points;
    }

    public SimpleStringProperty getDifficultyLvl() {
        return difficultyLvl;
    }

    public void setDifficultyLvlEnum(SimpleDoubleProperty difficultyLvlEnum) {
        this.difficultyLvl = difficultyLvl;
    }
}
