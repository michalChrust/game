package firstgame.database;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionMaker {

    //FIELDS
    private static Logger logger = LoggerFactory.getLogger(ConnectionMaker.class);
    private MysqlDataSource dataSource;

    //CONSTRUCTORS
    private ConnectionMaker(String fileName) {
        Properties dataBaseProperties = getDataBaseProperties(fileName);
        dataSource = getDataSource(dataBaseProperties);
    }

    public ConnectionMaker() {
        this("/databases/database.properties");
    }

    //GETTERS
    private Properties getDataBaseProperties(String filename) {
        Properties properties = new Properties();
        try {
            InputStream propertiesStream = ConnectionMaker.class.getResourceAsStream(filename);
            if (propertiesStream == null) {
                throw new IllegalArgumentException("Can't find file: " + filename);
            }
            properties.load(propertiesStream);
        } catch (IOException e) {
            logger.error("Erron during loading properties for firstgame.database");
            e.printStackTrace();
        }
        return properties;
    }

    public Connection getConnection() throws SQLException {
        if (dataSource == null) {
            throw new IllegalStateException("Data source is not created yet!");

        }
        dataSource.setAllowPublicKeyRetrieval(true);
        return dataSource.getConnection();
    }

    private MysqlDataSource getDataSource(Properties dataBaseProperties) {
        dataSource = new MysqlDataSource();
        try {
            dataSource.setServerName(dataBaseProperties.getProperty("pl.sda.jdbc.db.server"));
            dataSource.setDatabaseName(dataBaseProperties.getProperty("pl.sda.jdbc.db.name"));
            dataSource.setUser(dataBaseProperties.getProperty("pl.sda.jdbc.db.user"));
            dataSource.setPassword(dataBaseProperties.getProperty("pl.sda.jdbc.db.password"));
            dataSource.setPort(Integer.parseInt(dataBaseProperties.getProperty("pl.sda.jdbc.db.port")));
            dataSource.setServerTimezone("Europe/Warsaw");
            dataSource.setUseSSL(false);
            dataSource.setCharacterEncoding("UTF-8");
        } catch (SQLException e) {
            logger.error("Error during creating MysqlDataSource", e);
        }
        return dataSource;
    }
}
