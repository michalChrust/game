package firstgame.enums;

public enum DifficultyLvlEnum {
    EASY (0, 1000, 20, "Poziom Trudności (Łatwy)"),
    MEDIUM (0, 1100, 15, "Poziom Trudności (Średni)"),
    HARD (0, 1250, 10, "Poziom Trudności (Trudny)"),
    VERY_HARD (0 , 1500, 8,"Poziom Trudności (Bardzo trudny)"),
    CUSTOMIZED (0, 0, 0, "Poziom Trudności (Własny");

    final int lowerBound, upperBound, numberOfTries;
    final String difficultyLvl;

    DifficultyLvlEnum(int lowerBound, int upperBound, int numberOfTries, String difficultyLvl){
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.numberOfTries = numberOfTries;
        this.difficultyLvl = difficultyLvl;
    }

    public int getLowerBound() {
        return this.lowerBound;
    }

    public int getUpperBound() {
        return this.upperBound;
    }

    public int getNumberOfTries() {
        return this.numberOfTries;
    }

    public String getDifficultyLvl() {
        return this.difficultyLvl;
    }
}
