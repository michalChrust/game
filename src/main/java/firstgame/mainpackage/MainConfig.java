package firstgame.mainpackage;

import javafx.fxml.FXMLLoader;
import javafx.scene.ImageCursor;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MainConfig {

    //FIELDS
    private Logger logger = LoggerFactory.getLogger(MainConfig.class);

    //SETTERS
    void setCursor(String resourcePath, Scene scene) {
        logger.info("Cursor is set.");
        Image image = new Image(String.valueOf(getClass().getResource(resourcePath)));
        scene.setCursor(new ImageCursor(image));
    }

    void setStyle(Stage stage, String title, String resource) {
        logger.info("Style is set");
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle(title);
        Image image1 = new Image(String.valueOf(getClass().getResource(resource)));
        stage.getIcons().add(image1);
    }

    //OTHER METHODS
    void mainSceneSettings(Stage stage, Scene scene) {
        logger.info("Main scene settings are set");
        stage.setResizable(false);
        stage.setScene(scene);
    }

    Scene sceneCreator(String resource) throws IOException {
        StackPane loader = FXMLLoader.load(getClass().getResource(resource));
        return new Scene(loader);
    }
}
