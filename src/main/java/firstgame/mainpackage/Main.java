package firstgame.mainpackage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        MainConfig mainConfig = new MainConfig();
        Scene scene = mainConfig.sceneCreator("/fxml/mainControllerScene.fxml");
        mainConfig.setCursor("/graphics/793.png", scene);
        mainConfig.setStyle(stage, "Enigma", "/graphics/25400.png");
        mainConfig.mainSceneSettings(stage, scene);
        stage.show();
    }
}