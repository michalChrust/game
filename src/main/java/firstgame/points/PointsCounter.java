package firstgame.points;

import firstgame.enums.DifficultyLvlEnum;

public class PointsCounter {
    //FIELDS
    private DifficultyLvlEnum difficultyLvlEnum;
    //CONSTRUCTORS

    public PointsCounter(DifficultyLvlEnum difficultyLvlEnum) {
        this.difficultyLvlEnum = difficultyLvlEnum;
    }

    //OTHER METHODS
    public int whenWonCountPoints(int counter) {
        int finalPoints;

        finalPoints = this.difficultyLvlEnum.getNumberOfTries() - counter;
        switch (this.difficultyLvlEnum) {
            case EASY:
                finalPoints *= 10;
                finalPoints += 1100;
                break;
            case MEDIUM:
                finalPoints *= 15;
                finalPoints += 1250;
                break;
            case HARD:
                finalPoints *= 25;
                finalPoints += 1500;
                break;
            case VERY_HARD:
                finalPoints *= 40;
                finalPoints += 2000;
                break;
        }
        return finalPoints;
    }

    public int whenLostCountPoints(int lastTry, int result) {
        int finalPoints;
        int diffTemp = this.difficultyLvlEnum.getUpperBound() - Math.abs(result - lastTry);
        switch (this.difficultyLvlEnum) {
            case EASY:
                finalPoints = diffTemp * 1;
                break;
            case MEDIUM:
                finalPoints = diffTemp * 2;
                break;
            case HARD:
                finalPoints = diffTemp * 3;
                break;
            case VERY_HARD:
                finalPoints = diffTemp * 4;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + this.difficultyLvlEnum);
        }
        return finalPoints;
    }
}
