package firstgame.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Game {
    private static Logger logger = LoggerFactory.getLogger(Game.class);

    public Game() {
    }

    public boolean testIfNumberIsDigit(String result) {
        boolean digits = false;
        try {
            int number = Integer.parseInt(result);
            digits = true;
            return digits;
        } catch (Exception e) {
            logger.info("Player entered non-digit argument!");
        }
        return digits;
    }

    public boolean testIfNumberIsInRange(int number, int upperBound) {
        return number <= upperBound;
    }

    public int checkNumber(int result, int randomInt) {
        return Integer.compare(result, randomInt);
    }
}
