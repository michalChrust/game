package firstgame.game;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Player {

    //FIELDS
    private MediaPlayer mediaPlayer = null;
    private String songTitle;

    //CONTRUCTORS
    public Player(String songTitle) {
        this.songTitle = songTitle;
        setSong(songTitle);
    }

    //SETTERS
    private void setSong(String song) {
        this.mediaPlayer = new MediaPlayer(new Media(String.valueOf(getClass().getResource("/music/" + song))));
    }

    public void setVolume(double volume) {
        this.mediaPlayer.setVolume(volume);
    }

    public void setMute() {
        if (this.mediaPlayer.isMute()) {
            this.mediaPlayer.setMute(false);
            this.mediaPlayer.play();
        } else {
            this.mediaPlayer.setMute(true);
            this.mediaPlayer.stop();
        }
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }

    //GETTERS
    public double getVolume() {
        return this.mediaPlayer.getVolume();
    }

    public boolean getMute() {
        return this.mediaPlayer.isMute();
    }

    public String getSongTitle() {
        return songTitle;
    }

    //OTHER METHODS
    public void playMusic() {
        this.mediaPlayer.play();
    }

    public void changeMusic(String song) {
        double volume = this.mediaPlayer.getVolume();
        this.mediaPlayer.stop();
        setSong(song);
        this.mediaPlayer.setVolume(volume);
    }
}