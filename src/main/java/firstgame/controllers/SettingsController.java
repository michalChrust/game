package firstgame.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import firstgame.controllers.root.RootController;
import firstgame.enums.DifficultyLvlEnum;
import firstgame.game.Player;

public class SettingsController implements RootController {

    //FIELDS
    private MainController mainController;
    private Player player;
    @FXML
    private Button buttDifficultyLvl;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    private void setPlayerConfig(VoiceSettingsController voiceSettingsController, Player player) {
        voiceSettingsController.setPlayer(player);
        voiceSettingsController.setSliderMusic(player.getVolume());
        voiceSettingsController.setCheckBox();
        voiceSettingsController.setValueChoiceBox(player.getSongTitle());
    }

    void setWritting(DifficultyLvlEnum difficultyLvlEnum) {
        switch (difficultyLvlEnum) {
            case EASY:
                buttDifficultyLvl.setText("Poziom Trudności (Łatwy)");
                break;
            case MEDIUM:
                buttDifficultyLvl.setText("Poziom Trudności (Średni)");
                break;
            case HARD:
                buttDifficultyLvl.setText("Poziom Trudności (Trudny)");
                break;
            case VERY_HARD:
                buttDifficultyLvl.setText("Poziom Trudności (Bardzo trudny)");
                break;
            case CUSTOMIZED:
                buttDifficultyLvl.setText("Poziom Trudności (Własny)");
                break;
            default:
                System.out.println("błąd!");
        }
    }

    //START METHODS
    @FXML
    public void initialize() {
    }

    //OTHER METHODS
    @FXML
    void buttDifficultyLvlOnAction() {
        DifficultyLvlController difficultyLvlController = new DifficultyLvlController();
        difficultyLvlController = (DifficultyLvlController) mainController.setScreen("/fxml/difficultyLvlScene.fxml", difficultyLvlController);
        difficultyLvlController.setButtons();
    }

    @FXML
    void buttVoiceSettingsOnAction() {
        VoiceSettingsController voiceSettingsController = new VoiceSettingsController();
        voiceSettingsController = (VoiceSettingsController) mainController.setScreen("/fxml/voiceSettings.fxml", voiceSettingsController);
        player = mainController.getPlayer();
        setPlayerConfig(voiceSettingsController, player);
    }

    @FXML
    void buttBackOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainMenuController);
    }
}
