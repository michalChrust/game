package firstgame.controllers;

import firstgame.controllers.root.RootController;
import firstgame.database.ConnectionMaker;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ToSaveScoreController implements RootController {

    //FIELDS
    private MainController mainController;
    @FXML
    private TextField nickLabelId;
    private int points;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    //GETTERS
    private String getNickLabelText() {
        return nickLabelId.getText();
    }

    //START METHODS
    @FXML
    public void initalize() {
    }

    //OTHER METHODS
    @FXML
    public void buttAcceptOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainController);
        saveScore(nickLabelId.getText(), points, mainController.getDifficultyLvlEnum().getDifficultyLvl());
    }

    private void saveScore(String nick, double points, String difficultyLvl) {
        ConnectionMaker connectionMaker = new ConnectionMaker();
        try (Connection connection = connectionMaker.getConnection()) {
            PreparedStatement statementForNick = connection.prepareStatement("INSERT INTO scores (player_nick, points, difficulty_level) values (?, ?, ?)");
            statementForNick.setString(1, nick);
            statementForNick.setDouble(2, points);
            statementForNick.setString(3, difficultyLvl);
            statementForNick.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
