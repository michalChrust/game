package firstgame.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import firstgame.controllers.root.RootController;

public class WonEndGameController implements RootController {
    //FIELDS
    private MainController mainController;
    @FXML
    private Label randomNumberLabel, counterValueLabel, pointsValueLabel;

    private int points;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    void setRandomNumberLabel (String text){
        this.randomNumberLabel.setText(text);
    }

    void  setCounterValueLabel(String text){
        this.counterValueLabel.setText(text);
    }

    void setPointsValueLabel(String text){
        pointsValueLabel.setText(text);
    }


    //GETTERS
    Label getRandomNumber() {
        return randomNumberLabel;
    }

    Label getCounterValue() {
        return counterValueLabel;
    }


    //START METHODS
    @FXML
    public void initialize(){

    }

    //OTHER METHODS
    @FXML
    protected void saveScoreOnAction() {
        ToSaveScoreController toSaveScoreController = new ToSaveScoreController();
        toSaveScoreController = (ToSaveScoreController) mainController.setScreen("/fxml/toSaveScore.fxml", toSaveScoreController);
        toSaveScoreController.setPoints(points);
    }

    @FXML
    protected void backButtOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainMenuController);
    }
}
