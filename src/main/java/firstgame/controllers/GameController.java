package firstgame.controllers;

import
        javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import firstgame.controllers.root.RootController;
import firstgame.game.Game;
import firstgame.points.PointsCounter;

import java.util.Random;

public class GameController implements RootController {
    //CONTROLLERS
    public GameController() {
        this.upperBound = 0;
    }

    //FIELDS
    @FXML
    private TextField textField;
    @FXML
    private Label textLabel, labelWithRange;
    @FXML
    private ImageView imageCounter;
    @FXML
    private int
            upperBound,
            numberOfTries = 20,
            randomIntiger = 0,
            counter = 0;
    private MainController mainController;
    private Random random = new Random(System.currentTimeMillis());
    private Game game;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    void setNumberOfTries(int numberOfTries) {
        this.numberOfTries = numberOfTries;
    }

    void setRandomIntiger(int upperBound) {
        randomIntiger = random.nextInt(upperBound);
    }

    void setLabelWithRangeText(String text) {
        labelWithRange.setText(text);
    }

    void setCounter(int counter) {
        String counterS = "/numbers/" + (this.numberOfTries - counter) + ".png";
        Image image = new Image(String.valueOf(getClass().getResource(counterS)));
        imageCounter.setImage(image);
    }

    //START METHODS
    @FXML
    public void initialize() {
    }


    //OTHER METHODS
    private int countPoints() {
        PointsCounter pointsCounter = new PointsCounter(mainController.getDifficultyLvlEnum());
        return pointsCounter.whenWonCountPoints(counter);
    }

    @FXML
    void textFieldOnKeyPressed(KeyEvent e) {
        if (e.getCode() == KeyCode.ENTER) {
            buttAcceptOnAction();
        }
    }

    @FXML
    void buttAcceptOnAction() {
        String result = textField.getText();
        mainGameMethod(result);
        textField.requestFocus();
    }

    private void openWonScene() {
        WonEndGameController wonEndGameController = new WonEndGameController();
        wonEndGameController = (WonEndGameController) mainController.setScreen("/fxml/wonEndGame.fxml", wonEndGameController);
        wonEndGameController.setCounterValueLabel("Odgadłeś liczbę za " + counter + " razem!");
        wonEndGameController.setRandomNumberLabel(String.valueOf(randomIntiger));
        int points = countPoints();
        wonEndGameController.setPointsValueLabel("Liczba punktów: " + points);
        wonEndGameController.setPoints(points);
    }

    private void gameResult(int numberGiven, int randomNumber) {
        if (game.checkNumber(numberGiven, randomNumber) == 1) {
            textLabel.setText("Moja liczba jest mniejsza!");
            setCounter(counter++);
            textField.setText("");
        } else if (game.checkNumber(numberGiven, randomNumber) == -1) {
            textLabel.setText("Moja liczba jest większa!");
            setCounter(counter++);
            textField.setText("");
        } else {
            openWonScene();
        }
    }

    private void mainGameMethod(String result) {
        game = new Game();

        //testing if number is digit
        if (!game.testIfNumberIsDigit(result)) {
            textLabel.setText("To nie jest liczba!");
            textField.setText("");
            return;
        }

        int resultInt = Integer.parseInt(result);

        //testing if number is in range
        if (!game.testIfNumberIsInRange(resultInt, upperBound)) {
            textLabel.setText("Podałeś liczbę spoza zakresu!");
            return;
        }

        //testing if number of tries player done is lower than allowed number of tries
        if (!(counter < (numberOfTries))) {
            LostEndGameController lostEndGameController = new LostEndGameController();
            lostEndGameController = (LostEndGameController) mainController.setScreen("/fxml/lostEndGame.fxml", lostEndGameController);
            lostEndGameController.setLabelRandomIntText(randomIntiger);
            int points = countPoints();
            lostEndGameController.setLabelPointsId(points);
        }

        //giving game result
        gameResult(resultInt, randomIntiger);
    }
}
