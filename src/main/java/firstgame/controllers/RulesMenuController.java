package firstgame.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import firstgame.controllers.root.RootController;

public class RulesMenuController implements RootController {

    //FIELDS
    @FXML
    private TextArea textArea;
    private MainController mainController;


    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setRulesText() {
        textArea.setText("Zasada jest prosta. Ja losuję liczbę naturalną od " + mainController.getDifficultyLvlEnumLowerBound() + " do " + mainController.getDifficultyLvlEnumUpperBound() + " a Ty musisz ją zgadnąć, pojadąc kolejne liczby. Wskazówką z mojej strony będzie jedynie komunikat, czy moja liczba jest większa czy mniejsza. Masz " + mainController.getDifficultyLvlEnumNumberOfTries() + " szans. Gotowy?");
    }

    //START METHODS
    @FXML
    public void initialize() {
    }

    //OTHER METHODS
    private void setGameControllerValues(GameController gameController) {
        gameController.setLabelWithRangeText("<" + mainController.getDifficultyLvlEnumLowerBound() + "," + mainController.getDifficultyLvlEnumUpperBound() + ">");
        gameController.setUpperBound(mainController.getDifficultyLvlEnumUpperBound());
        gameController.setNumberOfTries(mainController.getDifficultyLvlEnumNumberOfTries());
        gameController.setRandomIntiger(mainController.getDifficultyLvlEnumUpperBound());
        gameController.setCounter(0);
    }

    @FXML
    void buttNextOnAction() {
        GameController gameController = new GameController();
        gameController = (GameController) mainController.setScreen("/fxml/game.fxml", gameController);
        setGameControllerValues(gameController);
    }

    @FXML
    void buttBackOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainMenuController);
    }
}
