package firstgame.controllers;

import firstgame.controllers.root.RootController;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Slider;
import firstgame.game.Player;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class VoiceSettingsController implements RootController {

    //FIELDS
    private MainController mainController;
    @FXML
    private Slider sliderMusic;
    @FXML
    private CheckBox checkBoxMusic;
    @FXML
    private ChoiceBox choiceBox;
    private Player player;
    private String song;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setPlayer(Player player) {
        this.player = player;
    }

    void setSliderMusic(double value) {
        sliderMusic.setValue(value);
    }

    void setCheckBox() {
        checkBoxMusic.setSelected(!player.getMute());
    }

    void setValueChoiceBox(String songName) {
        choiceBox.setValue(songName);
    }

    private void setChoiceBoxItems(List<String> items) {
        choiceBox.getItems().addAll(items);
    }

    //START METHODS
    @FXML
    public void initialize() throws IOException, URISyntaxException {
        List<String> lines = Files.readAllLines(Paths.get(this.getClass().getResource("/databases/songs/songs.txt").toURI()));
        setChoiceBoxItems(lines);

        sliderMusic.valueProperty().addListener(observable -> player.setVolume(sliderMusic.getValue()));

        choiceBox.valueProperty().addListener(observable -> {
            song = (String) choiceBox.getValue();
            if (!player.getSongTitle().equals(choiceBox.getValue())) {
                player.setSongTitle(song);
                player.changeMusic(song);
                player.playMusic();
            }
        });
    }

    //OTHER METHODS
    @FXML
    protected void checkBoxMusicOnAction() {
        player.setMute();
    }

    @FXML
    protected void buttBackOnAction() {
        SettingsController settingsController = new SettingsController();
        settingsController = (SettingsController) mainController.setScreen("/fxml/settings.fxml", settingsController);
        settingsController.setWritting(mainController.getDifficultyLvlEnum());
    }
}
