package firstgame.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import firstgame.controllers.root.RootController;

public class MainMenuController implements RootController {

    //FIELDS
    private MainController mainController;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    //START METHODS
    @FXML
    public void initialize() {
    }

    //OTHER METHODS
    @FXML
    void buttNewGameOnAction() {
        RulesMenuController rulesMenuController = new RulesMenuController();
        rulesMenuController = (RulesMenuController) mainController.setScreen("/fxml/rulesMenu.fxml", rulesMenuController);
        rulesMenuController.setRulesText();
    }

    @FXML
    void buttRankOnAction(){
        RankingController rankingController = new RankingController();
        rankingController = (RankingController) mainController.setScreen("/fxml/ranking.fxml", rankingController);
        rankingController.getRankElements();
    }

    @FXML
    private void buttOptionsOnAction() {
        SettingsController settingsController = new SettingsController();
        settingsController = (SettingsController) mainController.setScreen("/fxml/settings.fxml", settingsController);
        settingsController.setWritting(mainController.getDifficultyLvlEnum());
    }

    @FXML
    private void buttEndGameOnAction() {
        Platform.exit();
    }
}
