package firstgame.controllers;

import firstgame.controllers.root.RootController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.text.Font;
import javafx.util.Duration;
import firstgame.enums.DifficultyLvlEnum;

public class DifficultyLvlController implements RootController {
    public Font x1;
    //FIELDS
    private MainController mainController;
    @FXML
    private Button buttVeryHard, buttHard, buttMedium, buttEasy;
    private Tooltip tooltipEasy, tooltipMedium, tooltipHard, tooltipVeryHard;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setButtons() {
        tooltipEasy = new Tooltip();
        tooltipMedium = new Tooltip();
        tooltipHard = new Tooltip();
        tooltipVeryHard = new Tooltip();
        tooltipEasy.setShowDelay(Duration.millis(250));
        tooltipMedium.setShowDelay(Duration.millis(250));
        tooltipHard.setShowDelay(Duration.millis(250));
        tooltipVeryHard.setShowDelay(Duration.millis(250));
        tooltipEasy.setText("Liczba prób: 20 \nMinimalna liczba: 1 \nMaksymalna liczba: 1000");
        buttEasy.setTooltip(tooltipEasy);
        tooltipMedium.setText("Liczba prób: 15 \nMinimalna liczba: 1 \nMaksymalna liczba: 1100");
        buttMedium.setTooltip(tooltipMedium);
        tooltipHard.setText("Liczba prób: 10 \nMinimalna liczba: 1 \nMaksymalna liczba: 1250");
        buttHard.setTooltip(tooltipHard);
        tooltipVeryHard.setText("Liczba prób: 8 \nMinimalna liczba: 1 \nMaksymalna liczba: 1500");
        buttVeryHard.setTooltip(tooltipVeryHard);
    }

    //START METHODS
    @FXML
    public void initialize() {
    }

    //OTHER METHODS
    @FXML
    void buttVeryHardOnAction() {
        mainController.setDifficultyLvlEnum(DifficultyLvlEnum.VERY_HARD);
        buttBackOnAction();
    }

    @FXML
    void buttHardOnActionOnAction() {
        mainController.setDifficultyLvlEnum(DifficultyLvlEnum.HARD);
        buttBackOnAction();
    }

    @FXML
    void buttMediumOnActionOnAction() {
        mainController.setDifficultyLvlEnum(DifficultyLvlEnum.MEDIUM);
        buttBackOnAction();

    }

    @FXML
    void buttEasyOnActionOnAction() {
        mainController.setDifficultyLvlEnum(DifficultyLvlEnum.EASY);
        buttBackOnAction();
    }

    @FXML
    void buttBackOnAction() {
        SettingsController settingsController = new SettingsController();
        settingsController = (SettingsController) mainController.setScreen("/fxml/settings.fxml", settingsController);
        settingsController.setWritting(mainController.getDifficultyLvlEnum());
    }
}