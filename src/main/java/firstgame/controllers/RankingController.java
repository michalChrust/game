package firstgame.controllers;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import org.hibernate.Session;
import firstgame.controllers.root.RootController;
import firstgame.database.ConnectionMaker;
import firstgame.ranking.RankingEntity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class RankingController implements RootController {

    //FIELDS
    private MainController mainController;
    @FXML
    private TableView<RankingEntity> tableViewId;
    private List<RankingEntity> rankElementsList;
    private Connection connection;
    private ObservableList<RankingEntity> data = FXCollections.observableArrayList();

    //SETTERS
    @Override
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setConnection(Connection connection) {
        this.connection = connection;
    }

    //START METHOD
    public void initialize() {
        tableViewId.setItems(data);
        //column for nick
        TableColumn<RankingEntity, String> nickColumn = new TableColumn<>("Nick");
        nickColumn.setMinWidth(150);
        nickColumn.setCellValueFactory(rankElemStringCellDataFeatures -> rankElemStringCellDataFeatures.getValue().getNick());

        //column for score
        TableColumn<RankingEntity, Number> scoreColumn = new TableColumn<>("Punkty");
        scoreColumn.setMinWidth(150);
        scoreColumn.setCellValueFactory(rankElemNumberCellDataFeatures -> rankElemNumberCellDataFeatures.getValue().getPoints());

        //column for difficulty lvl
        TableColumn<RankingEntity, String> diffLvlColumn = new TableColumn<>("Poziom Trudności");
        diffLvlColumn.setMinWidth(150);
        diffLvlColumn.setCellValueFactory(rankElemNumberCellDataFeatures -> rankElemNumberCellDataFeatures.getValue().getDifficultyLvl());

        tableViewId.getColumns().addAll(nickColumn, scoreColumn, diffLvlColumn);
    }

    //OTHER METHODS
    @FXML
    public void backButtOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainMenuController);
    }

    List<RankingEntity> getRankElements() {
        ConnectionMaker connectionMaker = new ConnectionMaker();
        try (Connection connection = connectionMaker.getConnection()) {
            Statement statementForNick = connection.createStatement();
            Statement statementForPoints = connection.createStatement();
            Statement statementForDiffLvl = connection.createStatement();
            ResultSet resultNameSet = statementForNick.executeQuery("SELECT player_nick FROM scores");
            ResultSet resultPointsSet = statementForPoints.executeQuery("SELECT points FROM scores");
            ResultSet resultDiffLvlSet = statementForDiffLvl.executeQuery("SELECT difficulty_level FROM scores");
            while ((resultNameSet.next() && resultPointsSet.next()) && resultDiffLvlSet.next()) {
                data.add(new RankingEntity(resultNameSet.getString("player_nick"), resultPointsSet.getDouble("points"), resultDiffLvlSet.getString("difficulty_level")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rankElementsList;
    }
}
