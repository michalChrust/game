package firstgame.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import firstgame.controllers.root.RootController;

public class LostEndGameController implements RootController {
    //FIELDS
    private MainController mainController;
    private int randomInt;
    private int points;
    @FXML
    private Label labelRandomInt, labelPointsId;

    //SETTERS
    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    void setLabelRandomIntText(int randomInt) {
        this.randomInt = randomInt;
        labelRandomInt.setText("Wylosowana liczba to: " + randomInt);
    }

    void setLabelPointsId(int points){
        this.points = points;
        labelPointsId.setText("Liczba punktów: " + points);
    }

    //START METHODS
    public void initialize() {
    }

    //OTHER METHODS
    @FXML
    void savePointsButtOnAction() {
        ToSaveScoreController toSaveScoreController = new ToSaveScoreController();
        toSaveScoreController = (ToSaveScoreController) mainController.setScreen("/fxml/toSaveScore.fxml", toSaveScoreController);
        toSaveScoreController.setPoints(points);
    }

    @FXML
    void menuBackButtOnAction() {
        MainMenuController mainMenuController = new MainMenuController();
        mainMenuController = (MainMenuController) mainController.setScreen("/fxml/mainMenu.fxml", mainMenuController);
    }
}
