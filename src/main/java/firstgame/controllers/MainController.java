package firstgame.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;
import firstgame.controllers.root.RootController;
import firstgame.enums.DifficultyLvlEnum;
import firstgame.game.Player;

import java.io.IOException;

public class MainController implements RootController {

    //FIELDS
    @FXML
    private StackPane mainStackPane;
    private DifficultyLvlEnum difficultyLvlEnum;
    private Player player;

    //CONTRUCTOR
    public MainController() {
        this.difficultyLvlEnum = DifficultyLvlEnum.EASY;
        this.player = new Player("GameOn.mp3");
        player.playMusic();
        player.setVolume(0.2);
    }

    //GETTERS
    DifficultyLvlEnum getDifficultyLvlEnum() {
        return this.difficultyLvlEnum;
    }

    int getDifficultyLvlEnumLowerBound() {
        return difficultyLvlEnum.getLowerBound();
    }

    int getDifficultyLvlEnumUpperBound() {
        return difficultyLvlEnum.getUpperBound();
    }

    int getDifficultyLvlEnumNumberOfTries() {
        return difficultyLvlEnum.getNumberOfTries();
    }

    Player getPlayer() {
        return player;
    }

    //SETTERS
    private void setScene(StackPane stackPane) {
        mainStackPane.getChildren().clear();
        mainStackPane.getChildren().add(stackPane);
    }

    void setDifficultyLvlEnum(DifficultyLvlEnum difficultyLvlEnum) {
        this.difficultyLvlEnum = difficultyLvlEnum;
    }

    //START METHODS
    @FXML
    public void initialize() {
        setScreen("/fxml/mainMenu.fxml", new RulesMenuController());
    }

    //OTHER METHODS
    RootController setScreen(String resource, RootController rootController) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(resource));
        StackPane stackPane = null;
        try {
            stackPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        rootController = loader.getController();
        rootController.setMainController(this);
        setScene(stackPane);
        return rootController;
    }
}
