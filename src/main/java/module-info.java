module firstGame{
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.media;
    requires java.naming;
    requires slf4j.api;
    requires mysql.connector.java;
    requires java.sql;
    requires java.persistence;
    requires org.hibernate.orm.core;


    exports firstgame.mainpackage to javafx.graphics;
    exports firstgame.controllers to javafx.fxml;

    opens firstgame.controllers to javafx.fxml;
}